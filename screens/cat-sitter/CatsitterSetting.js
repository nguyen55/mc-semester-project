import React, { useEffect, useState, useLayoutEffect } from "react";
import { View, TouchableOpacity, Text, TextInput, StyleSheet, Image, Switch, SafeAreaView, ScrollView } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { FontAwesome } from "@expo/vector-icons";
import colors from "../../colors";
import { signOut } from "firebase/auth";
import { AntDesign } from "@expo/vector-icons";
import { collection, query, onSnapshot, where, and, updateDoc, doc } from "firebase/firestore";
import { auth, database } from "../../config/firebase";
import { DIMENSION_WIDTH } from "../../assets/styles";
import CurrencyInput from 'react-native-currency-input';

//When user has role cat sitter
//TODO: get profile of cat sitter => If not then has to create
//Cat sitter has 2 screens => Profile Setting and Chat List
//Check for some needed info => If not go direct to setting screen
//Include Chatbox like in cat owner home => OK
const nameStyle = [
    {
        paddingTop: 10,
        paddingBottom: 5,
        color: "#363636",
        fontSize: 15
    }
];

const CatsitterSetting = () => {
    const navigation = useNavigation();
    const [catsitter, setCatsitter] = useState({})
    const [profileId, setProfileId] = useState("")

    const onSignOut = () => {
        signOut(auth).catch((error) => console.log("Error logging out: ", error));
    };

    useEffect(() => {
        navigation.setOptions({
            headerLeft: () => <FontAwesome name="home" size={24} color={colors.gray} style={{ marginLeft: 15 }} />,
            headerRight: () => (
                <TouchableOpacity
                    style={{
                        marginRight: 10
                    }}
                    onPress={onSignOut}>
                    <AntDesign name="logout" size={24} color={colors.gray} style={{ marginRight: 10 }} />
                </TouchableOpacity>
            ),
            title: "Setup your profile"
        });
    }, [navigation]);

    useLayoutEffect(() => {
        const id = auth?.currentUser?.uid;

        if (!id) {
            navigate("/")
            return
        };
        const collectionRef = collection(database, "profiles");
        const q = query(collectionRef, and(where("userId", "==", id)),);

        const unsubscribe = onSnapshot(q, (querySnapshot) => {
            if (querySnapshot.docs.length > 0) {
                setProfileId(querySnapshot.docs[0].id)
                setCatsitter({
                    id: querySnapshot.docs[0].id,
                    ...querySnapshot.docs[0].data()
                })

            }
        });
        return unsubscribe;
    }, []);

    const onUpdateProfileClick = async () => {
        if (!profileId) return;
        const profileRef = doc(database, "profiles", profileId);
        const updateProfile = await updateDoc(profileRef, { ...catsitter, updatedAt: new Date() });
        alert(updateProfile)
    }

    return (
        <SafeAreaView>
            <ScrollView style={styles.scrollView}>
                <View source={require("../../assets/images/10.jpg")} style={styles.containerHomePage}>
                    <View>
                        <View style={{
                            alignItems: "center",
                            margin: 10,
                            padding: 10,
                            width: "95%",
                            display: "flex",
                            flexDirection: "column",
                            overflow: "hidden",
                            borderWidth: 0
                        }}>
                            {/* Open for search */}
                            <View style={{ width: "100%", display: "flex", flexDirection: "row", justifyContent: "space-between" }}>
                                <Text style={styles.heading2}> Open for catsitter-search</Text>
                                <Switch
                                    trackColor={{ false: '#767577', true: '#81b0ff' }}
                                    ios_backgroundColor="#3e3e3e"
                                    onValueChange={(isOpen) => setCatsitter({ ...catsitter, isOpen })}
                                    value={catsitter.isOpen}
                                />
                            </View>

                            {/* IMAGE */}
                            <View style={{ width: "100%" }}>
                                <Text style={styles.heading2}> Display avatar:</Text>
                                <Image source={{ uri: catsitter.avatar }} style={[
                                    {
                                        borderRadius: 8,
                                        height: 120,
                                        margin: 0,
                                        aspectRatio: 1,
                                        borderWidth: 2,
                                        borderColor: "#FAFAFA"
                                    }
                                ]} />

                            </View>

                            {/* Display Name */}
                            <View style={[styles.inputContainer, { marginTop: 20 }]}>
                                <Text style={styles.inputLabel}>Your display name:
                                </Text >
                                <TextInput
                                    style={styles.input}
                                    placeholder="Max Mustermann"
                                    autoCapitalize="words"
                                    autoCorrect={false}
                                    textContentType="name"
                                    value={catsitter.displayName}
                                    onChangeText={(text) => setCatsitter({ ...catsitter, displayName: text })}
                                />
                            </View>

                            {/* Biography */}
                            <View style={styles.inputContainer}>
                                <Text style={styles.inputLabel}>Biography:
                                </Text >
                                <TextInput
                                    style={[styles.input, { height: "auto" }]}
                                    placeholder="Write a short description about you"
                                    autoCapitalize="sentences"
                                    autoCorrect={false}
                                    numberOfLines={4}
                                    multiline={true}
                                    editable
                                    value={catsitter.description}
                                    onChangeText={(text) => setCatsitter({ ...catsitter, description: text })}
                                />
                            </View>

                            {/* Address and postal code */}
                            <View style={styles.inputContainer}>
                                <Text style={styles.inputLabel}>Your Address:
                                </Text >
                                <View style={{ display: "flex", flexDirection: "row", gap: 10 }}>
                                    <TextInput
                                        style={[styles.input, { width: "33%" }]}
                                        placeholder="72762"
                                        autoCapitalize="none"
                                        autoCorrect={false}
                                        textContentType="postalCode"
                                        value={catsitter.postalCode}
                                        onChangeText={(text) => setCatsitter({ ...catsitter, postalCode: text })}
                                    />

                                    <TextInput
                                        style={[styles.input, { flexGrow: 1 }]}
                                        placeholder="Reutlingen"
                                        autoCapitalize="none"
                                        autoCorrect={false}
                                        textContentType="addressCity"
                                        value={catsitter.address}
                                        onChangeText={(text) => setCatsitter({ ...catsitter, address: text })}
                                    />
                                </View>
                            </View>

                            {/* Age */}
                            <View style={styles.inputContainer}>
                                <Text style={styles.inputLabel}>Your Age (optional):
                                </Text >
                                <TextInput
                                    style={styles.input}
                                    placeholder="Address"
                                    autoCapitalize="none"
                                    autoCorrect={false}
                                    textContentType="streetAddressLine1"
                                    value={catsitter.age}
                                    onChangeText={(text) => setCatsitter({ ...catsitter, age: text })}
                                />
                            </View>

                            {/* Available time: */}
                            <View style={styles.inputContainer}>
                                <Text style={styles.inputLabel}>Your available time:
                                </Text >
                                <TextInput
                                    style={[styles.input]}
                                    placeholder="13:00 - 15:00"
                                    autoCapitalize="none"
                                    autoCorrect={false}
                                    value={catsitter.availableTime}
                                    onChangeText={(text) => setCatsitter({ ...catsitter, availableTime: text })}
                                />
                            </View>

                            {/* Price per hour */}
                            <View style={styles.inputContainer}>
                                <Text style={[styles.inputLabel]}>Price per hour:
                                </Text >
                                <CurrencyInput value={catsitter.price}
                                    onChangeValue={(text) => setCatsitter({ ...catsitter, price: text })} style={styles.input} />
                            </View>

                        </View>

                        <TouchableOpacity style={styles.buttonContainer} onPress={onUpdateProfileClick}>
                            <Text style={styles.button}>Update your profile</Text>
                        </TouchableOpacity>
                    </View>


                </View>
            </ScrollView>
        </SafeAreaView >
    );
};

export default CatsitterSetting;


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#fff",
        position: "relative"
    },
    chatButton: {
        position: "absolute",
        bottom: 0,
        right: 0,
        backgroundColor: colors.primary,
        height: 50,
        width: 50,
        borderRadius: 25,
        alignItems: "center",
        justifyContent: "center",
        shadowColor: colors.primary,
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.9,
        shadowRadius: 8,
        marginRight: 20,
        marginBottom: 50
    },
    //Home
    heading1: {
        color: "#444240",
        fontSize: 20,
        fontWeight: "500",
        letterSpacing: 1,
        marginVertical: 20,
        marginHorizontal: 14
    },
    heading2: {
        color: "#444240",
        fontSize: 16,
        fontWeight: "400",
        letterSpacing: 1,
        marginVertical: 10,
    },
    inputContainer: {
        display: "flex",
        flexDirection: "column",
        width: "100%"
    },
    inputLabel: {
        marginVertical: 6,
        fontSize: 14,
        fontWeight: "400",
    },
    input: {
        backgroundColor: "#F6F7FB",
        height: 58,
        marginBottom: 20,
        fontSize: 16,
        borderRadius: 10,
        padding: 12
    },
    cchat: {
        display: "flex",
        flexDirection: "row",
        width: DIMENSION_WIDTH - 40,
        marginHorizontal: 20,
        padding: 16,
        marginVertical: 4,
        backgroundColor: "#FAFAFA",
        shadowColor: colors.primary,
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.9,
        shadowRadius: 8,
        borderRadius: 12,
    },
    buttonContainer: {
    },
    button: {
        fontWeight: "bold",
        color: "#fff",
        fontSize: 18,
        backgroundColor: "#7444C0",
        margin: 20,
        paddingHorizontal: 20,
        textAlign: "center",
        paddingVertical: 10,
        borderRadius: 10
    }
});
