import React, { useEffect, useState, useLayoutEffect } from "react";
import { View, TouchableOpacity, Text, Pressable, FlatList } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { FontAwesome } from "@expo/vector-icons";
import colors from "../../colors";
import { collection, query, onSnapshot, orderBy, where } from "firebase/firestore";
import styles from "../../assets/styles";
import { signOut } from "firebase/auth";
import { AntDesign } from "@expo/vector-icons";
import { auth, database } from "../../config/firebase";
import ChatComponent from "../../components/ChatComponent"


//When user has role cat sitter
//TODO: get profile of cat sitter => If not then has to create
// If not updatedAt => go profile setting
//Cat sitter has 2 screens => Profile Setting and Chat List
//Check for some needed info => If not go direct to setting screen
//Include Chatbox like in cat owner home => OK
const CatsitterHome = () => {
    const navigation = useNavigation();
    const [chats, setChats] = useState([]);
    const [profiles, setProfiles] = useState([])

    const onSignOut = () => {
        signOut(auth).catch((error) => console.log("Error logging out: ", error));
    };

    useEffect(() => {
        navigation.setOptions({
            headerLeft: () => <FontAwesome name="home" size={24} color={colors.gray} style={{ marginLeft: 15 }} />,
            headerRight: () => (
                <TouchableOpacity
                    style={{
                        marginRight: 10
                    }}
                    onPress={onSignOut}>
                    <AntDesign name="logout" size={24} color={colors.gray} style={{ marginRight: 10 }} />
                </TouchableOpacity>
            )
        });
    }, [navigation]);

    useLayoutEffect(() => {
        const collectionRef = collection(database, "chats");
        const q = query(collectionRef, where("participant", "array-contains", auth.currentUser.uid), orderBy("updatedAt", "desc"));

        const unsubscribe = onSnapshot(q, (querySnapshot) => {
            setChats(
                querySnapshot.docs.map((doc) => ({
                    _id: doc.id,
                    content: doc.data()
                }))
            );
        });
        return unsubscribe;
    }, []);

    return (
        <View style={styles.container}>
            <View>
                {chats.length > 0 ? (
                    <FlatList
                        data={chats.map((chat) => {
                            //Find participant id
                            const participant_id = chat.content.participant.find(p => p !== auth?.currentUser.uid)

                            //Find participant
                            const participant = profiles.find(p => p.data.userId == participant_id);
                            return ({
                                id: chat._id,
                                name: participant ? participant.data.displayName : undefined,
                                avatar: participant ? participant.data.avatar : undefined,
                            })
                        })}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item }) => (
                            <Pressable onPress={() => navigation.navigate("ChatDetail", { id: item.id })} style={styles.cchat}>
                                <ChatComponent item={item} />
                            </Pressable>
                        )}
                    />
                ) : (<View>
                    <Text style={styles.heading1}>Currently no chat available. Configure your profile to gain more attention</Text>
                </View>)}
            </View>
        </View>
    );
};

export default CatsitterHome;


