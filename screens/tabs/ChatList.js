import React, { useEffect, useState, useLayoutEffect } from "react";
import { View, TouchableOpacity, Image, StyleSheet, FlatList, Pressable, Text } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { FontAwesome } from "@expo/vector-icons";
import colors from "../../colors";
import { collection, query, onSnapshot, orderBy, where } from "firebase/firestore";
import { database, auth } from "../../config/firebase";
import ChatComponent from "../../components/ChatComponent"
import { DIMENSION_WIDTH } from "../../assets/styles";

const catImageUrl =
  "https://i.guim.co.uk/img/media/26392d05302e02f7bf4eb143bb84c8097d09144b/446_167_3683_2210/master/3683.jpg?width=1200&height=1200&quality=85&auto=format&fit=crop&s=49ed3252c0b2ffb49cf8b508892e452d";

const ChatList = () => {
  const navigation = useNavigation();
  const [chats, setChats] = useState([]);
  const [profiles, setProfiles] = useState([])

  useEffect(() => {
    navigation.setOptions({
      headerLeft: () => <FontAwesome name="search" size={24} color={colors.gray} style={{ marginLeft: 15 }} />,
      headerRight: () => (
        <Image
          source={{ uri: catImageUrl }}
          style={{
            width: 40,
            height: 40,
            marginRight: 15
          }}
        />
      )
    });
  }, [navigation]);

  useLayoutEffect(() => {
    const collectionRef = collection(database, "chats");
    const q = query(collectionRef, where("participant", "array-contains", auth.currentUser.uid));

    const unsubscribe = onSnapshot(q, (querySnapshot) => {
      setChats(
        querySnapshot.docs.map((doc) => ({
          _id: doc.id,
          content: doc.data()
        }))
      );
    });
    return unsubscribe;
  }, []);

  useEffect(() => {
    if (chats.length < 1) return
    //Get all participants
    const allParticipants = chats.reduce((acc, obj) => {
      return acc.concat(obj.content.participant);
    }, []);

    if (allParticipants.length < 1) return;

    const catsittersCollectionRef = collection(database, "profiles");
    const q = query(catsittersCollectionRef, where("userId", "in", allParticipants));

    const unsubscribe = onSnapshot(q, (querySnapshot) => {
      setProfiles(
        querySnapshot.docs.map((doc) => ({
          id: doc.id,
          data: doc.data()
        }))
      );
    });
    return unsubscribe;

  }, [chats])

  return (
    <View style={styles.container}>
      <View>
        {chats.length > 0 ? (
          <FlatList
            data={chats.map((chat) => {
              //Find participant id
              const participant_id = chat.content.participant.find(p => p !== auth?.currentUser.uid)

              //Find participant
              const participant = profiles.find(p => p.data.userId == participant_id);
              return ({
                id: chat._id,
                name: participant ? participant.data.displayName : undefined,
                avatar: participant ? participant.data.avatar : undefined,
              })
            })}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => (
              <Pressable onPress={() => navigation.navigate("ChatDetail", { id: item.id })} style={styles.cchat}>
                <ChatComponent item={item} />
              </Pressable>
            )}
          />
        ) : <Text style={{ textAlign: "right" }}>No Chat available</Text>}
      </View>
    </View>
  );
};

export default ChatList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
    position: "relative"
  },
  chatButton: {
    position: "absolute",
    bottom: 0,
    right: 0,
    backgroundColor: colors.primary,
    height: 50,
    width: 50,
    borderRadius: 25,
    alignItems: "center",
    justifyContent: "center",
    shadowColor: colors.primary,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.9,
    shadowRadius: 8,
    marginRight: 20,
    marginBottom: 50
  },
  input: {
    backgroundColor: "#F6F7FB",
    height: 58,
    marginBottom: 20,
    fontSize: 16,
    borderRadius: 10,
    padding: 12
  },
  cchat: {
    display: "flex",
    flexDirection: "row",
    width: DIMENSION_WIDTH - 40,
    marginHorizontal: 20,
    padding: 16,
    marginVertical: 4,
    backgroundColor: "#FAFAFA",
    shadowColor: colors.primary,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.9,
    shadowRadius: 8,
    borderRadius: 12,
    borderColor: "#EDE6DD"
  },
});
