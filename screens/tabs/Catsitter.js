import { Text, View, Dimensions, TouchableOpacity, Image, Pressable } from "react-native";
import React, { useState, useLayoutEffect } from "react";
import { collection, query, onSnapshot, where, and } from "firebase/firestore";
import { database } from "../../config/firebase";
import Icon from "../../components/Icon";
import styles, {
  DISLIKE_ACTIONS,
  LIKE_ACTIONS,
  WHITE,
} from "../../assets/styles";
import { useNavigation } from "@react-navigation/native";


const Catsitter = ({ route }) => {
  const navigation = useNavigation()
  const { id } = route.params;

  const [catsitter, setCatsitter] = useState({});
  const [catsitterId, setCatsitterId] = useState("")

  useLayoutEffect(() => {
    navigation.setOptions({
      title: catsitter.displayName
    });
  }, [catsitter]);

  useLayoutEffect(() => {
    if (!id) return;
    const collectionRef = collection(database, "profiles");
    const q = query(collectionRef, and(where("__name__", "==", id), where("role", "==", "cat-sitter")),);

    const unsubscribe = onSnapshot(q, (querySnapshot) => {
      setCatsitter(querySnapshot.docs[0].data());
    });
    return unsubscribe;
  }, []);

  const fullWidth = Dimensions.get("window").width;

  const imageStyle = [
    {
      borderRadius: 8,
      width: catsitter.hasVariant ? fullWidth / 2 - 30 : fullWidth - 80,
      height: catsitter.hasVariant ? 170 : 350,
      margin: catsitter.hasVariant ? 0 : 20,
    },
  ];

  const nameStyle = [
    {
      paddingTop: catsitter.hasVariant ? 10 : 15,
      paddingBottom: catsitter.hasVariant ? 5 : 7,
      color: "#363636",
      fontSize: catsitter.hasVariant ? 15 : 30,
    },
  ];


  return (
    <>
      <View style={{
        borderRadius: 8,
        alignItems: "center",
        margin: 10,
        elevation: 1,
        padding: 10,
        width: "95%",
        display: "flex",
        flexDirection: "column",
        overflow: "hidden"
      }}>
        {/* IMAGE */}
        <Image source={{ uri: catsitter.avatar }} style={imageStyle} />

        {/* MATCHES */}
        <View style={styles.matchesCardItem}>
          <Text style={styles.matchesTextCardItem}>
            <Icon name="heart" color={WHITE} size={13} /> {catsitter.availableTime}
          </Text>
        </View>
        <Text >Address:{catsitter.postalCode} {catsitter.address}</Text >


        {/* NAME */}
        <Text style={nameStyle}>{catsitter.displayName}</Text>
        <Text >Cat Sitter Price Per Night: {catsitter.price}</Text >

        {/* DESCRIPTION */}
        <Text style={styles.descriptionCardItem}>{catsitter.description}</Text>

        {/* STATUS */}
        <View style={styles.status}>
          <View style={catsitter.isOpen ? styles.online : styles.offline} />
          <Text style={styles.statusText}>
            {catsitter.isOpen ? "Online" : "Offline"}
          </Text>
        </View>

        {/* ACTIONS */}
        <View style={styles.actionsCardItem}>
          <Pressable>
            <TouchableOpacity style={styles.button} onPress={() => navigation.navigate("ChatWithUser", { id: catsitter.userId })}>
              <Icon name="mail" color={LIKE_ACTIONS} size={25} />
            </TouchableOpacity>
          </Pressable>

        </View>
      </View>
    </>
  );
};

export default Catsitter;

