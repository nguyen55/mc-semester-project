import React, { useEffect, useState, useLayoutEffect } from "react";
import { View, TouchableOpacity, Text, TextInput, FlatList, ImageBackground, Keyboard, Button, PRETT } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { FontAwesome } from "@expo/vector-icons";
import colors from "../../colors";
import { collection, query, onSnapshot, orderBy, where, and } from "firebase/firestore";
import { Entypo } from "@expo/vector-icons";
import { CardItem } from "../../components";
import { Icon } from '../../components'
import styles from "../../assets/styles";
import { signOut } from "firebase/auth";
import { BLACK, WHITE } from '../../assets/styles'
import { AntDesign } from "@expo/vector-icons";
import { auth, database } from "../../config/firebase";


//When user has role catowner
const CatownerHome = () => {
  const navigation = useNavigation();
  const [catsitters, setCatsitters] = useState([]);
  const [keyword, setKeyword] = useState("");

  const onSignOut = () => {
    signOut(auth).catch((error) => console.log("Error logging out: ", error));
  };

  useEffect(() => {
    navigation.setOptions({
      headerLeft: () => <FontAwesome name="home" size={24} color={colors.gray} style={{ marginLeft: 15 }} />,
      headerRight: () => (
        <TouchableOpacity
          style={{
            marginRight: 10
          }}
          onPress={onSignOut}>
          <AntDesign name="logout" size={24} color={colors.gray} style={{ marginRight: 10 }} />
        </TouchableOpacity>
      )
    });
  }, [navigation]);

  useLayoutEffect(() => {
    const collectionRef = collection(database, "profiles");
    const q = query(collectionRef, and(where("role", "==", "cat-sitter"), where("isOpen", "==", true)), orderBy("updatedAt", "desc"));

    const unsubscribe = onSnapshot(q, (querySnapshot) => {
      setCatsitters(
        querySnapshot.docs.map((doc) => ({
          _id: doc.id,
          content: doc.data()
        }))
      );
    });
    return unsubscribe;
  }, []);

  return (
    <View source={require("../../assets/images/10.jpg")} style={styles.containerHomePage}>
      <TouchableOpacity style={{ width: "100%" }}>
        <Text style={styles.heading1}>Cat sitter near your home</Text>
      </TouchableOpacity>
      <View style={{ display: "flex", flexDirection: "row", alignItems: "center", width: "100%", paddingHorizontal: 14 }}>
        <Text style={styles.heading2}>Your Location: </Text>
        <TouchableOpacity style={styles.currentCity}>
          <Icon name="navigate-outline" size={20} color={WHITE} />
          <Text style={styles.textButton}>Reutlingen</Text>
        </TouchableOpacity>
      </View>

      <View style={styles.searchBarContainer}>
        <Icon name="search-outline" size={20} color={BLACK} />
        <TextInput style={styles.searchInput} placeholder="Search" autoCapitalize="none" autoFocus={false} value={keyword} onChangeText={(text) => setKeyword(text)} />
      </View>

      <FlatList style={styles.flatListContainer}
        data={catsitters}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) => (
          <TouchableOpacity onPress={() => navigation.navigate("Catsitter", { id: item._id })}>
            <CardItem description={item.content.description}
              hasActions={true}
              hasVariant={true}
              avatar={item.content.avatar}
              isOnline={true}
              location={item.content.address}
              price={item.content.price}
              name={item.content.displayName}
              onPressSend={() => navigation.navigate("ChatWithUser", { uid: item.content.userId })}
              style={styles.cchat}
            />
          </TouchableOpacity>
        )}
      />

      <TouchableOpacity onPress={() => navigation.navigate("Chat")} style={styles.chatButton}>
        <Entypo name="chat" size={24} color={colors.lightGray} />
      </TouchableOpacity>
    </View>


  );
};

export default CatownerHome;


