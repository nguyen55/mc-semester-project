import React, { useState, useLayoutEffect, useCallback, useEffect } from "react";
import { TouchableOpacity } from "react-native";
import { GiftedChat } from "react-native-gifted-chat";
import { collection, addDoc, orderBy, query, onSnapshot, doc, where, or } from "firebase/firestore";
import { signOut } from "firebase/auth";
import { auth, database } from "../../config/firebase";
import { useNavigation } from "@react-navigation/native";
import { AntDesign } from "@expo/vector-icons";
import colors from "../../colors";

export default function ChatUid({ route }) {
    const { uid } = route.params;

    const [id, setId] = useState("")

    const chatCollectionRef = collection(database, "chats");
    const messageCollectionRef = collection(database, "messages");

    const [messages, setMessages] = useState([]);
    const navigation = useNavigation();

    const onSignOut = () => {
        signOut(auth).catch((error) => console.log("Error logging out: ", error));
    };

    useLayoutEffect(() => {
        navigation.setOptions({
            headerRight: () => (
                <TouchableOpacity
                    style={{
                        marginRight: 10
                    }}
                    onPress={onSignOut}>
                    <AntDesign name="logout" size={24} color={colors.gray} style={{ marginRight: 10 }} />
                </TouchableOpacity>
            )
        });
    }, [navigation]);

    //Get chat id
    useLayoutEffect(() => {
        if (!uid) return;
        const q = query(chatCollectionRef, or(where("participant", "==", [uid, auth.currentUser.uid]), where("participant", "==", [auth.currentUser.uid, uid])));

        const unsubscribe = onSnapshot(q, (querySnapshot) => {
            if (querySnapshot.docs.length == 0) {
                //Create chat
                addDoc(chatCollectionRef, {
                    createdAt: new Date(),
                    participant: [uid, auth.currentUser.uid]
                }).then((docRef) => {
                    setId(docRef.id)
                })
            } else setId(querySnapshot.docs[0]?.id);
        });
        return unsubscribe;
    }, []);

    useEffect(() => {
        if (!id) return;

        const chatRef = doc(database, "chats", id);

        const q = query(messageCollectionRef, where("chatId", "==", chatRef), orderBy("createdAt", "desc"));

        const unsubscribe = onSnapshot(q, (querySnapshot) => {
            setMessages(
                querySnapshot.docs.map((doc) => ({
                    _id: doc.id,
                    createdAt: doc.data().createdAt.toDate(),
                    text: doc.data().text,
                    user: doc.data().user
                }))
            );
        });
        return unsubscribe;
    }, [id]);

    const onSend = (messages = []) => {
        if (!id) alert("System error");
        const chatRef = doc(database, "chats", id);

        setMessages((previousMessages) => GiftedChat.append(previousMessages, messages));
        const { createdAt, text, user } = messages[0];
        addDoc(messageCollectionRef, {
            createdAt,
            text,
            user,
            chatId: chatRef
        });
    };

    return (
        <>
            <GiftedChat
                messages={messages}
                showAvatarForEveryMessage={false}
                showUserAvatar={false}
                onSend={(messages) => onSend(messages)}
                messagesContainerStyle={{
                    backgroundColor: "#fff",
                }}
                textInputStyle={{
                    backgroundColor: "#fff",
                    borderRadius: 20
                }}
                user={{
                    _id: auth?.currentUser?.uid,
                    name: auth?.currentUser?.displayName,
                    avatar: auth?.currentUser?.avatar
                }}
            />
        </>
    );
}
