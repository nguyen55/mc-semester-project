import React, { useState, useLayoutEffect } from "react";
import { auth, database } from "../config/firebase";
import { collection, query, where, and, addDoc, getDocs } from "firebase/firestore";
import { createStackNavigator } from "@react-navigation/stack";
import Chat from "./tabs/Chat";
import ChatUid from "./tabs/ChatUid";
import CatownerHome from "./tabs/CatownerHome";
import ChatList from "./tabs/ChatList";
import Catsitter from "./tabs/Catsitter";
import CatsitterHome from "./cat-sitter/CatsitterHome";
import CatsitterSetting from "./cat-sitter/CatsitterSetting";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { View, Text, Image, ImageBackground, Icon } from "react-native";
import { Ionicons } from '@expo/vector-icons';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const loadingImage =
    "https://technometrics.net/wp-content/uploads/2020/11/loading-icon-animated-gif-19-1.gif";

const ScreenNavigator = () => {
    //Get current profile
    const [userProfile, setUserProfile] = useState({});
    useLayoutEffect(() => {
        const id = auth?.currentUser?.uid;

        if (!id) {
            navigate("/")
            return
        };
        const collectionRef = collection(database, "profiles");
        const q = query(collectionRef, and(where("userId", "==", id)),);
        getDocs(q).then((data) => {
            if (data.docs.length > 0) {
                setUserProfile(data.docs[0].data())
            } else {
                createUserProfile(auth?.currentUser?.uid)
            }
        });
    }, []);

    const createUserProfile = (userId) => {
        addDoc(collection(database, "profiles"), {
            createdAt: new Date(),
            userId: userId,
            role: "cat-owner"
        });
    }
    if (userProfile.role == "cat-owner") return (
        <Stack.Navigator defaultScreenOptions={CatownerHome}>
            <Stack.Screen name="CatownerHome" component={CatownerHome} />
            <Stack.Screen name="Catsitter" component={Catsitter} />
            <Stack.Screen name="Chat" component={ChatList} />
            <Stack.Screen name="ChatDetail" component={Chat} />
            <Stack.Screen name="ChatWithUser" component={ChatUid} />
        </Stack.Navigator>
    )
    if (userProfile.role == "cat-sitter") return (<Tab.Navigator>
        {/* Cat sitter stack */}
        <Tab.Screen name="CatsitterHome" component={CatsitterHome} options={{
            tabBarLabel: "Home", tabBarIcon: ({ color, size }) => (
                <Ionicons name="home-outline" color={color} size={size} />
            ),
        }} />
        <Tab.Screen name="CatsitterSetting" component={CatsitterSetting} options={{
            tabBarLabel: "Setting", tabBarIcon: ({ color, size }) => (
                <Ionicons name="options-outline" color={color} size={size} />
            ),
        }} />
    </Tab.Navigator>
    )
    return (
        <View>
            <ImageBackground
                source={{ uri: loadingImage }}
                style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    width: 300,
                    height: 400,
                }}
            />
        </View>
    )

}

export default ScreenNavigator