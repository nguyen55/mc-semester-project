import React from "react";
import { Text, View, Image, Dimensions, TouchableOpacity, Pressable } from "react-native";
import Icon from "./Icon";
import styles, { DISLIKE_ACTIONS, GRAY } from "../assets/styles";

export type CardItemT = {
  description?: string;
  hasActions?: boolean;
  hasVariant?: boolean;
  avatar: any;
  isOnline?: boolean;
  location?: string;
  name: string;
  price: string;
  onPressSend: () => void;
};

const CardItem = ({ description, hasActions, hasVariant, avatar, isOnline, location, name, price, onPressSend }: CardItemT) => {
  // Custom styling
  const fullWidth = Dimensions.get("window").width;

  const imageStyle = [
    {
      borderRadius: 8,
      height: hasVariant ? 120 : fullWidth - 80,
      margin: hasVariant ? 0 : 20,
      aspectRatio: 1,
      borderWidth: 2,
      borderColor: "#FAFAFA"
    }
  ];

  const nameStyle = [
    {
      paddingTop: hasVariant ? 10 : 15,
      paddingBottom: hasVariant ? 5 : 7,
      color: "#363636",
      fontSize: hasVariant ? 15 : 30
    }
  ];

  return (
    <View style={styles.containerCardItem}>
      {/* IMAGE */}
      <View style={{}}>
        <Image source={{ uri: avatar }} style={imageStyle} />
        {/* MATCHES */}
        {location && (
          <View style={styles.matchesCardItem}>
            <Text style={styles.matchesTextCardItem}>{location}</Text>
          </View>
        )}
        <Text style={{ textAlign: "right" }}>{price}€/Std.</Text>
      </View>

      <View style={{ height: "100%", flexGrow: 1, padding: 10, width: "90%" }}>
        {/* NAME */}
        <Text style={nameStyle}>{name}</Text>

        {/* DESCRIPTION */}
        {description && (
          <Text style={styles.descriptionCardItem} numberOfLines={4}>
            {description}
          </Text>
        )}

        {/* STATUS */}
        {!description && (
          <View style={styles.status}>
            <View style={isOnline ? styles.online : styles.offline} />
            <Text style={styles.statusText}>{isOnline ? "Online" : "Offline"}</Text>
          </View>
        )}

        {/* ACTIONS */}
        {hasActions && (
          <Pressable style={styles.actionsCardItem}>
            <TouchableOpacity style={styles.button} onPress={onPressSend}>
              <Icon name="send" color={DISLIKE_ACTIONS} size={25} />
            </TouchableOpacity>
          </Pressable>
        )}
      </View>
    </View>
  );
};

export default CardItem;
