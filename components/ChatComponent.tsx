import { View, Text, Image } from "react-native";
import React, { useLayoutEffect, useState } from "react";
import styles from "../assets/styles";

const ChatComponent = ({ item }) => {
  const [message, setMessage] = useState<{ text: string; time: string }>(undefined);

  //👇🏻 Retrieves the last message in the array from the item prop
  useLayoutEffect(() => {
    setMessage(item.message);
  }, []);

  return (
    <>
      <Image source={{ uri: item.avatar }} style={styles.cavatar} />

      <View style={styles.crightContainer}>
        <View>
          <Text style={styles.cusername}>{item.name}</Text>

          <Text style={styles.cmessage}>{message?.text ? message.text : "Tap to start chatting"}</Text>
        </View>
      </View>
    </>
  );
};

export default ChatComponent;
