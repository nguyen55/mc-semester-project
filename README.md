# catMe App using React Native Expo and Firebase
An Application created by Group 11. 
- catMe simplifies the process of finding reliable caregivers for cats when owners 
are away.
- Cat owner post vacation dates, and experienced cat sitters respond based 
on availability, allowing owners to choose based on factors like experience, 
cost, and proximity.

## How to use

Required basic reading skill to use this project

Clone the repo

```
git clone 
```

cd into the just created project and install dependencies with yarn

```
cd ChatApp && yarn
```

Add your firebase backend config in the `firebase.js` file

```
const firebaseConfig = {
  apiKey: Constants.expoConfig.extra.apiKey,
  authDomain: Constants.expoConfig.extra.authDomain,
  projectId: Constants.expoConfig.extra.projectId,
  storageBucket: Constants.expoConfig.extra.storageBucket,
  messagingSenderId: Constants.expoConfig.extra.messagingSenderId,
  appId: Constants.expoConfig.extra.appId,
  databaseURL: Constants.expoConfig.extra.databaseURL,
  //   @deprecated is deprecated Constants.manifest
};

or add it in .env file
```

Run the project

```
expo start
```
if you are using public network, use expo start --tunnel instead

Congratulations 🎉 Now you have a functional catMe working locally

## Known issues

Expo SDK and libreries are always updating their versions and deprecating others. before installing the libreries run.

```
yarn add expo@latest
```

Next you can run:

```
    npx expo install --fix
```

Older versions of `react-native-gifted-chat` have a some issues. make sure you have the latest.

```
npx expo install react-native-gifted-chat@latest
```

Expo will show you what dependencies need to be updated. Install the dependencies expo suggest you. It is possible that there is cache and you have to run.

```
yarn start --reset-cache
```
Required basic reading skill to execute project